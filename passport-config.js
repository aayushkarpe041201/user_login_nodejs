import bcrypt from "bcrypt";
import { Strategy as LocalStrategy } from "passport-local";

const inititalize = (passport, getUserByEmial, getUserById) => {
  const authenticateUser = async (userName, password, done) => {
    const user = getUserByEmial(userName);
    if (user == null) {
      return done(null, false, { message: "No user with that User Name" });
    }

    try {
      if (await bcrypt.compare(password, user.password)) {
        return done(null, user);
      } else {
        return done(null, false, { message: "Password incorrect" });
      }
    } catch (e) {
      return done(e);
    }
  };

  passport.use(new LocalStrategy({ usernameField: "userName" }, authenticateUser));

  passport.serializeUser((user, done) => done(null, user.id));
  passport.deserializeUser((id, done) => {
    return done(null, getUserById(id));
  });
};

export default inititalize;
