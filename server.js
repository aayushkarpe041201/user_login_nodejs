import colors from "colors";
import express from "express";
import dotenv from "dotenv";
import bcrypt from "bcrypt";
import passport from "passport";
import flash from "express-flash";
import session from "express-session";
import methodOverride from "method-override";

import inititalizePassport from "./passport-config.js";

// users array which will contain all the user objects once they are registered on the register page
const users = [];

// all the configs required for the application
const PORT = process.env.PORT || 5000;

if (process.env.NODE_ENV !== "production") {
  dotenv.config();
}

inititalizePassport(
  passport,
  (userName) => users.find((user) => user.userName === userName),
  (id) => users.find((user) => user.id === id)
);

// app
const app = express();

app.set("view-engine", "ejs");
app.use(express.static("views"));
app.use(express.urlencoded({ extended: false }));
app.use(flash());
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
  })
);
app.use(passport.initialize());
app.use(passport.session());
app.use(methodOverride("_method"));

// all the redirects and application functions

function checkAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  return res.redirect("/login");
}

function checkNotAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return res.redirect("/");
  }

  next();
}

app.get("/", checkAuthenticated, (req, res) => {
  res.render("welcome.ejs", { name: req.user.name });
});

app.get("/login", checkNotAuthenticated, (req, res) => {
  res.render("login.ejs");
});

app.post(
  "/login",
  checkNotAuthenticated,
  passport.authenticate("local", {
    successRedirect: "/",
    failureRedirect: "/login",
    failureFlash: true,
  })
);

app.get("/register", checkNotAuthenticated, (req, res) => {
  res.render("register.ejs");
});

const alphanumericRegex = /^[a-zA-Z0-9]+$/;
const passwordRegex =
  /^(?=.*[a-zA-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$/;

app.post("/register", checkNotAuthenticated, async (req, res) => {
  try {
    const { name, userName, password } = req.body;

    // Validate username
    if (
      !alphanumericRegex.test(userName) ||
      userName.length < 6 ||
      userName.length > 12
    ) {
      req.flash(
        "error",
        "Username must be alphanumeric and between 6 to 12 characters long."
      );
      return res.redirect("/register");
    }

    // Validate password
    if (!passwordRegex.test(password)) {
      req.flash(
        "error",
        "Password must contain at least one alphabet, one number, one special character, and be at least 6 characters long."
      );
      return res.redirect("/register");
    }

    const hashedPassword = await bcrypt.hash(password, 10);

    users.push({
      id: Date.now().toString(),
      name,
      userName,
      password: hashedPassword,
    });
    res.redirect("/login");
  } catch (error) {
    console.log(error);
    res.redirect("/register");
  }
});

app.delete("/logout", (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      console.log(err);
    }
    res.redirect("/login");
  });
});

app.listen(PORT, () => {
  console.log(
    `Server running in ${process.env.NODE_ENV} mode on ${PORT}`.bold.yellow
  );
});
